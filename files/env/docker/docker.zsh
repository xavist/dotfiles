alias dps='docker ps'
alias ds='docker stop $(docker ps -a -q)';

alias di='docker images'

alias dl='docker logs'
alias dlf='docker logs -f'

alias dnp='docker network prune'
alias dsp='docker system prune'
alias dspa='docker system prune -a'
