export MAVEN_OPTS="-Xmx2g -XX:MaxMetaspaceSize=512m"

alias m='mvn verify -T 1C -DskipTests'
alias mc='mvn clean -T 1C'
alias mt='mvn verify -T 1C'
alias mcv='mvn clean verify -T 1C -DskipTests'
alias mcvt='mvn clean install -T 1C'

alias mw='./mvnw verify -T 1C -DskipTests'
alias mwc='./mvnw clean -T 1C'
alias mwt='./mvnw verify -T 1C'
alias mwcv='./mvnw clean verify -T 1C -DskipTests'
alias mwcvt='./mvnw clean install -T 1C'

alias mci='mvn clean install -T 1C -DskipTests'
alias mcit='mvn clean install -T 1C'

alias mwci='./mvnw clean install -T 1C -DskipTests'
alias mwcit='./mvnw clean install -T 1C'

alias mdep='mvn dependency:tree'
alias mwdep='./mvnw dependency:tree'

alias muv='mvn versions:update-properties && mvn versions:use-latest-versions'
alias mwuv='./mvnw versions:update-properties && ./mvnw versions:use-latest-versions'
