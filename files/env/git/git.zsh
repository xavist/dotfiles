alias gmb='git symbolic-ref refs/remotes/origin/HEAD | cut -d'/' -f4'

alias g='git'
alias gd='git diff'
alias gt='git status'
alias gm='git merge '
alias gr='git rebase '

alias guh='git reset HEAD'
alias gup='git reset HEAD~'
alias guhh='git reset --hard HEAD'
alias guph='git reset --hard HEAD~'

alias gf='git fetch'
alias gfp='git pull --rebase --autostash'

alias gb='git branch --color -v'
alias gc='git checkout '
alias gcb='git checkout -b '
alias gcm='git checkout $(gmb)'

alias ga='git add '
alias gci='git commit -m '
alias gcia='git commit -am '
alias gam='git commit --amend'
alias gama='git commit -a --amend'

alias gp='git push'
alias gpf='git push --force'

alias gcp='git cherry-pick '
alias gcpx='git cherry-pick -x '

alias gs='git stash'
alias gss='git stash save '
alias gsc='git stash clear'
alias gsl='git stash list'
alias gsp='git stash pop'
alias gsa='git stash apply'

alias gl='git log --graph --pretty=format:'\''%Cred%h%Creset - %Cgreen[%an]%Creset%C(yellow)%d%Creset: %s %Cgreen(%cr)%Creset'\'' --abbrev-commit --date=relative'

alias gmm='git checkout $(gmb) && git pull && git checkout - && git merge -'
alias grm='git checkout $(gmb) && git pull && git checkout - && git rebase -'
