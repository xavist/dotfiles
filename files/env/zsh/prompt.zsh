# Battery based on https://github.com/spaceship-prompt/spaceship-prompt/blob/master/sections/battery.zsh

BATTERY_SYMBOL_CHARGING="${BATTERY_SYMBOL_CHARGING="%F{yellow}⌁%f"}"
BATTERY_SYMBOL_DISCHARGING="${BATTERY_SYMBOL_DISCHARGING="%F{red}⌁%f"}"
BATTERY_SYMBOL_FULL="${BATTERY_SYMBOL_FULL="%F{green}⌁%f"}"
BATTERY_THRESHOLD_SHOW="${BATTERY_THRESHOLD_SHOW=75}"
BATTERY_THRESHOLD_WARN="${BATTERY_THRESHOLD_WARN=50}"
BATTERY_THRESHOLD_CAUTION="${BATTERY_THRESHOLD_CAUTION=25}"

function prompt_xavi_batt() {
  local battery_data battery_percent battery_status

  if command_exists pmset; then
    battery_data=$(pmset -g batt | grep "InternalBattery")

    # Return if no internal battery
    [[ -z "$battery_data" ]] && return

    # Colored output from pmset will break prompt if grep is aliased to show colors
    battery_percent="$( echo $battery_data | \grep -oE '[0-9]{1,3}%' )"
    battery_status="$( echo $battery_data | awk -F '; *' '{ print $2 }' )"
  elif command_exists acpi; then
    battery_data=$(acpi -b 2>/dev/null | head -1)

    # Return if no battery
    [[ -z $battery_data ]] && return

    battery_status_and_percent="$(echo $battery_data |  sed 's/Battery [0-9]*: \(.*\), \([0-9]*\)%.*/\1:\2/')"
    battery_status_and_percent_array=("${(@s/:/)battery_status_and_percent}")
    battery_status=$battery_status_and_percent_array[1]:l
    battery_percent=$battery_status_and_percent_array[2]

	# If battery is 0% charge, battery likely doesn't exist.
    [[ $battery_percent == "0" ]] && return

  elif command_exists upower; then
    local battery=$(command upower -e | grep battery | head -1)

    # Return if no battery
    [[ -z $battery ]] && return

    battery_data=$(upower -i $battery)
    battery_percent="$( echo "$battery_data" | grep percentage | awk '{print $2}' )"
    battery_status="$( echo "$battery_data" | grep state | awk '{print $2}' )"
  else
    return
  fi

  # Remove trailing % and symbols for comparison
  battery_percent="$(echo $battery_percent | tr -d '%[,;]')"

  if [[ $battery_percent -gt $BATTERY_THRESHOLD_SHOW ]]; then
    return
  fi

  # Change color based on battery percentage
  # Escape % for display since it's a special character in Zsh prompt expansion
  if [[ $battery_percent -gt $BATTERY_THRESHOLD_WARN ]]; then
    battery_percent="%F{green}$battery_percent%%%f"
  elif [[ $battery_percent -gt $BATTERY_THRESHOLD_CAUTION ]]; then
    battery_percent="%F{yellow}$battery_percent%%%f"
  else
    battery_percent="%F{red}$battery_percent%%%f"
  fi

  # Battery indicator based on current status of battery
  if [[ $battery_status == "charging" ]];then
    battery_symbol="${BATTERY_SYMBOL_CHARGING}"
  elif [[ $battery_status =~ "^[dD]ischarg.*" ]]; then
    battery_symbol="${BATTERY_SYMBOL_DISCHARGING}"
  else
    battery_symbol="${BATTERY_SYMBOL_FULL}"
  fi

  _prompt_xavi_batt="$battery_percent$battery_symbol "
}

function prompt_xavi_duration {
  zstyle ':zim:duration-info' threshold 5
  zstyle ':zim:duration-info' format '%F{yellow}%d%f '

  add-zsh-hook preexec duration-info-preexec
  add-zsh-hook precmd duration-info-precmd

  _prompt_xavi_duration='${duration_info}'
}

function prompt_xavi_git {
  zstyle ':zim:git-info:action' format ':%s'
  zstyle ':zim:git-info:ahead' format '%F{yellow}%A%%B⇡%%b%f' #Use dashed arrows cause normal ones look different between them
  zstyle ':zim:git-info:behind' format '%F{yellow}%%B⇣%%b%f%F{yellow}%B%f' #Redefine color for %B cause otherwise for some reason it gets reset
  zstyle ':zim:git-info:branch' format '%b'
  zstyle ':zim:git-info:commit' format '%c'
  zstyle ':zim:git-info:dirty' format '%D'
  zstyle ':zim:git-info:position' format '%p'
  zstyle ':zim:git-info:remote' format '%F{green}%%B=%%b%f'
  zstyle ':zim:git-info:stashed' format '%F{magenta}*%f'
  zstyle ':zim:git-info:untracked' format '%F{magenta}?%f'

  zstyle ':zim:git-info:keys' format \
    'prompt' '%F{$(to_color "%D" "%A%B")}$(coalesce "%b" "%p" "%c")%s%f[$(coalesce "%A%B" "%R" "%F{red}∙%f")]%u%S'

  add-zsh-hook precmd git-info
  _prompt_xavi_git='${(e)git_info[prompt]}'
}

function prompt_xavi_perm {
  _prompt_xavi_perm=':'

  if [[ ! -w "${PWD}" ]]; then
    _prompt_xavi_perm='%F{red}:%f'
  fi
}

function prompt_xavi_pwd {
  local left right max
  local pwd="${PWD/$HOME/~}"

  (( max = $COLUMNS/2 ))

  if (( $#pwd > $max )); then
    (( left = $max/2 ))
    (( right = $#pwd - $left ))
    _prompt_xavi_pwd="%F{blue}${${pwd:0:$left}%/*}/.../${${pwd:$right}#*/}%f"
  else
    _prompt_xavi_pwd="%F{blue}$pwd%f"
  fi
}

function to_color {
  if [[ -n $argv[1] ]]; then
    print 'red'
  elif [[ -n $argv[2] ]]; then
    print 'yellow'
  else
    print 'green'
  fi
}

setopt nopromptbang prompt{cr,percent,sp,subst}

autoload -Uz add-zsh-hook

_prompt_xavi_prefix='╭─'
_prompt_xavi_user='%(!.%F{red}.%F{green})%n%f'
_prompt_xavi_host='${SSH_TTY:+"@%m"}'

add-zsh-hook precmd prompt_xavi_perm
add-zsh-hook precmd prompt_xavi_pwd
prompt_xavi_git
prompt_xavi_duration
add-zsh-hook precmd prompt_xavi_batt

_prompt_xavi_postfix=$'\n╰─%(?.➤.%F{red}➤%f) '

_prompt_xavi_error='%(?..%F{red}%?↵  %f)'
_prompt_xavi_jobs='%1(j.%F{magenta}[%j&] %f.)'
_prompt_xavi_time='%F{blue}%* %f'

PROMPT="${_prompt_xavi_prefix}${_prompt_xavi_user}${_prompt_xavi_host}\${_prompt_xavi_perm}\${_prompt_xavi_pwd} ${_prompt_xavi_git}${_prompt_xavi_postfix}"
RPROMPT="${_prompt_xavi_error}${_prompt_xavi_duration}${_prompt_xavi_jobs}\${_prompt_xavi_batt}${_prompt_xavi_time}"
SPROMPT='zsh: correct %F{red}%R%f to %F{green}%r%f [nyae]? '
