#zsh extras
autoload -U zmv #Easy multiple file move/renaming

#zsh options
setopt NO_BEEP #Don't beep
setopt HIST_IGNORE_ALL_DUPS
#unsetopt SHARE_HISTORY #Don't share history between terminal tabs.
setopt CORRECT #Prompt for spelling correction of commands.

WORDCHARS=${WORDCHARS//[\/]} #Remove path separator from WORDCHARS.

#bind keys
issue-branch() {
  zle -U "${${__GIT_INFO_BRANCH##*/}%%_*} " #Using env var from zim
}

zle -N issue-branch
bindkey '^[i' issue-branch #Alt+i to insert JIRA issue key from branch name in commit messages
bindkey -s '\er' 'reload\n' #Alt+r to reload config (see alias)
bindkey -e #Set editor default keymap to emacs (`-e`) or vi (`-v`)

#alias
alias mkdir='mkdir -p'
alias o=open
alias psg='ps -ef | grep -v grep | grep '
alias reload='exec zsh'
alias rm='rm -i'
