# DOTFILES #

Actually, this repo is more than dotfiles.
It also helps you to backup and setup your Mac from scratch however you want by configuring a series of *actions*
that can have many *steps* which in turn can execute many *commands*.
If needed, is easy to define and contribute your own actions and commands.

**WARNING**

Aside from executing (sourcing) scripts as defined in the configuration, configuration files are also sourced as scripts,
so make sure you know what's in your config and what you run.

## Installing ##

Dependencies:

 - [Zsh](http://zsh.sourceforge.net/)

This repo is intended to be forked so that you can create your own config. Then just clone.
Recommended folder to clone is `~/.dotfiles` as some code regarding the repo currently assumes that.

## Usage ##

### Actions ###

`./dotfiles [-d] <action>`

`-d` means dry run.

For example, you could run `./dotfiles -d backup`.

Actions can define functions to execute before each step, but you can also add your configuration to it.

The best you can do to understand what an action does is to do a dry run.

### Configuration ###

Configuration files locations are defined by `DOTFILES_FILES_DIR` and `DOTFILES_PRIVATE_FILES_DIR` variables
which by default have the following values:

  - `DOTFILES_FILES_DIR`: `$DOTFILES_DIR/files` (`DOTFILES_DIR` will be the folder where this repository is cloned).
  - `DOTFILES_PRIVATE_FILES_DIR`: `~/.config/dotfiles`.

Configuration files are key/value pairs, must have `.cfg` extension, can have blank lines and can have comments by using `#`.

All configuration files should have the following key/value pairs:

  - `id`: identifier. Also used by some commands as the default thing to act upon.
  - `name`: human readable name about what this configuration is about.

The rest of key/value pairs are action and command configurations and work as follows:

  - *action_step=commands*: defines what commands to execute for the given *action* and *step*.
  - *action_step_command=arguments*: defines what arguments to pass to the given *command* (for *action* and *step*).
  If a *command* is only executed by a single *action* and *step*, this can be simplified to *command=arguments*.

Both commands and arguments can be a single value or multiple values, in which case they need to go between parenthesis
and separated by a space. Values can be delimited by single or double quotes.

Finally, each command can have `-Y` or `-N` appended to it's name in which case you will get asked to confirm
if you want run that command with `Y` or `N` as the default option as specified.

Take a look at my configuration files for some examples.

## Backup action ##

Not intended as a full backup of your disk or for big files.
Just for config files, SSH keys, those temp files you keep on your Desktop and maybe a few small repos... as
everything gets put in a `tar.gz` file so that there are no issues with hidden files.

Variables:

  - `DOTFILES_BACKUP_DIR`: temporary folder where files are copied. By default `/tmp/dotfiles/backup`.
  - `DOTFILES_BACKUP_FILE`: backup file. By default `~/Desktop/backup.tar.gz`.

Steps:

  - *setup*: get everything ready and ask for confirmation to proceed.
  - *copy*: copy files according to configuration to `DOTFILES_BACKUP_DIR`.
  - *tar*: create `DOTFILES_BACKUP_FILE` from `DOTFILES_BACKUP_DIR`.
  Will also offer to create a compressed tar file for the `~/.dotfiles` folder itself.
  - *clean*: cleanup. Currently remove `DOTFILES_BACKUP_DIR`.

## Install action ##

What this action does:

  - Install stuff with [Homebrew](https://brew.sh/).
  - Restore files from backup.
  - Create links.
  - Run/source scripts.

Variables:

  - `DOTFILES_BACKUP_FILE`: backup file. By default `~/Desktop/backup.tar.gz`.

Steps:

  - *base*: install the very basic stuff you need for everything else, for example brew.
  - *setup*: get everything ready and ask for confirmation to proceed.
  - *pre*: things to do/install before restoring backup.
  - *restore*: restore backup to your home directory.
  - *post*: things to do/install after restoring backup.
  - *clean*: cleanup. Currently does nothing.

## Available commands ##

  - *brew*: `brew install`
  - *cask*: `brew install --cask`
  - *copy*:tm copies stuff. Expects file patterns relative to `DOTFILES_COPY_SOURCE_DIR`
  (must be an absolute path and by default is your home folder) as it's arguments (although you can give absolute paths too).
  The files that match any of the patterns will be copied to `DOTFILES_COPY_TARGET_DIR` (by default is `DOTFILES_BACKUP_DIR`).
  See the command code for details.
  - *link*: links stuff. By default `.lnk` files as a `.` file with the same name without extension in your home folder.
  See the command code for details.
  - *mise*: `mise use --global`
  - *services*: `brew services`
  - *source*: source scripts. By default `.zshs` files whose name ends with *action-step*. See the command code for details.
  - *tap*: `brew tap`

## Notes on specific apps/libraries/stuff ##

### Preferences files ###

  - Preferences files are copied correctly, but don't always seem to work as this really depends on the applications

## Development ##

Actions, steps and commands MUST NOT contain spaces AND have valid zsh variable names.

### TODO ###

  - Do I need to explicitly install xcode-select in Mojave? (never needed to, was prompted to do so when required).
  - Add tests for all commands (for brew at least check arguments are correct by overriding brew with a function).
  - Allow to define brew prefix and use it as needed
  and/or have a function that returns it and can be used as needed in sourced scripts.
  - Add a command to add manual steps to review later.
  - Make sure all config steps of the same thing are/can be together.
  - Allow configs to provide reusable checks for their things (e.g. wait for Docker to start).
  - Add actions to create/list configs.
  - Make things idempotent, check if installed, skip if done or ask if overwrite...
  so that actions can be reexecuted without issues.
  - Make things more configurable without having to change scripts or maybe just ask more questions. Examples:
    - Which shell to set as default.
    - Copy bash profile/configs anyway.
    - Where to clone repos.
    - Have valid configs for lots of stuff but just easily enable what you want somehow.
