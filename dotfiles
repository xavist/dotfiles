#!/usr/bin/env zsh

readonly DOTFILES_DIR=${0:A:h}

source $DOTFILES_DIR/lib/zx/zx.zshs
source $DOTFILES_DIR/lib/zx/checks.zshs

def_option 'DOTFILES_DRY_RUN' $ZX_KO
def_dir 'DOTFILES_FILES_DIR' "$DOTFILES_DIR/files"
def_dir 'DOTFILES_PRIVATE_FILES_DIR' "$HOME/.config/dotfiles"

function _dotfiles_is_function {
  (( $+functions[$argv[1]] ))
}

function _dotfiles_is_dry_run {
  return $DOTFILES_DRY_RUN
}

function _dotfiles_import {
  local folder=$argv[1]; shift
  local item=$argv[1]; shift

  if _dotfiles_is_function "dotfiles_${folder}_${item}"; then
    return
  fi

  source $DOTFILES_DIR/src/$folder/$item.zshs "$@"
}

function _dotfiles_section {
  title "\n$argv[1]..."
}

function _dotfiles_confirm_or_exit {
  if ! confirm "$argv[1]" "Y"; then
    exit 0
  fi
}

function _dotfiles_load_config {
  dotfiles_config___path=${~argv[1]:A:h} # Special config variable (_) so that files can be defined relatively
  source <(awk '!/^($|[:space:]*#)/ { print "dotfiles_config_" $0 }' $argv[1]) # The initial expression skips empty lines and comments
}

function _dotfiles_unload_config {
  unset -m "dotfiles_config_*"
}

function _dotfiles_get_relative_path {
  local _var="$argv[1]" _path="$argv[2]" _file="$argv[3]"
  assign "$_var" "${_file#$_path/}"
}

function _dotfiles_get_config_values {
  local _var="$argv[1]"; shift
  local _context; _context=($@) # Remove empty arguments, so that joining by underscores below is correct
  local _config_var="dotfiles_${(j:_:)_context}"
  eval $_var='("${(e)${(P)_config_var}[@]}")'
}

function _dotfiles_get_command_args_with_prefix {
  local args_var="$argv[1]" prefix="$argv[2]" cmd="$argv[3]" action="$argv[4]" step="$argv[5]"

  _dotfiles_get_config_values "$args_var" "$prefix" "$action" "$step" "$cmd"

  if ! empty "${(P)args_var}"; then
    return
  fi

  _dotfiles_get_config_values "$args_var" "$prefix" "$cmd"
}

function _dotfiles_get_command_args {
  local args_var="$argv[1]"; shift

  _dotfiles_get_command_args_with_prefix "$args_var" 'config' "$argv[@]"

  if ! empty "${(P)args_var}"; then
    return
  fi

  _dotfiles_get_command_args_with_prefix "$args_var" 'default' "$argv[@]"

  if ! empty "${(P)args_var}"; then
    return
  fi

  _dotfiles_get_config_values "$args_var" 'config' 'id'
}

function _dotfiles_confirm_command {
  local cmd="$argv[1]"; shift
  local confirm="$argv[1]"; shift

  if _dotfiles_is_function "dotfiles_commands_${cmd}_confirm"; then
    dotfiles_commands_${cmd}_confirm "$confirm" "$argv[@]"
  else
    confirm "Do you want to run <dotfiles_commands_${cmd} $args[@]>" "$confirm"
  fi
}

function _dotfiles_run_command {
  local args cmd="${argv[1]%-*}" confirm="${argv[1]##*-}" action="$argv[2]" step="$argv[3]"

  _dotfiles_import 'commands' "$cmd"
  _dotfiles_get_command_args 'args' "$cmd" "$action" "$step"

  case "$confirm" in
      y|Y|n|N)
        if ! _dotfiles_confirm_command "$cmd" "$confirm" "$args[@]"; then
          return
        fi
  esac

  if _dotfiles_is_dry_run; then
    info "DRY RUN: dotfiles_commands_${cmd} $args[@]"
  else
    info "Running <dotfiles_commands_${cmd} $args[@]>"
    dotfiles_commands_${cmd} "$args[@]"
  fi
}

function _dotfiles_run_action_step_function {
  local action="$argv[1]"; shift
  local step="$argv[1]"; shift

  _dotfiles_section "Running $step step"

  if _dotfiles_is_function "dotfiles_actions_${action}_${step}"; then
    if _dotfiles_is_dry_run; then
      info "DRY RUN: dotfiles_actions_${action}_${step}"
    else
      dotfiles_actions_${action}_${step}
    fi
  fi
}

function _dotfiles_run_action {
  local step steps config cmd cmds
  local action="$argv[1]"; shift
  steps=(${argv:-''})

  for step in "$steps[@]"; do
    _dotfiles_run_action_step_function "$action" "$step"

    for config in $DOTFILES_FILES_DIR/**/*.cfg(N) $DOTFILES_PRIVATE_FILES_DIR/**/*.cfg(N); do #(N) means don't throw an error if no matches found
      _dotfiles_load_config "$config"

      _dotfiles_get_config_values 'cmds' 'config' "$action" "$step"

      for cmd in $cmds; do
        _dotfiles_run_command "$cmd" "$action" "$step"
      done

      _dotfiles_unload_config
    done
  done
}

function _dotfiles_check_args {
  if empty "$argv[1]"; then
    error "No action specified"
    exit
  fi
}

function dotfiles_main {
  local action="$argv[1]"; shift

  if equal "$action" '-d'; then
    _dotfiles_section "DRY RUN"
    DOTFILES_DRY_RUN=$ZX_OK
    _dotfiles_check_args "$argv[1]"
    action="$argv[1]"; shift
  fi

  _dotfiles_section "Running <$action> action"
  _dotfiles_import 'actions' "$action" "$@"
  dotfiles_actions_${action}
}

# If the script is not being sourced, execute command
if equal "$ZSH_EVAL_CONTEXT" 'toplevel'; then
  _dotfiles_check_args "$@"
  dotfiles_main "$@"
fi
